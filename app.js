function contadorRegressivo() {
  var valor = document.querySelector("#caixaTexto").value;
  var tamTexto = 140 - valor.length;

  document.querySelector("#numero").innerHTML = tamTexto;

  if (tamTexto <= 10) {
    document.querySelector("#numero").style.color = "red";
  } else if (tamTexto <= 20) {
    document.querySelector("#numero").style.color = "green";
  } else {
    document.querySelector("#numero").style.color = "white";
  }

  if (!valor || valor.length >= 140 || valor.match(/^[ \t\n\r\f\v]+$/)) {
    document.querySelector("button").disabled = true;
  } else {
    document.querySelector("button").disabled = false;
  }
  
  document
    .querySelector("#caixaTexto")
    .addEventListener("onkeydown", areaTexto());
  document.querySelector("#caixaTexto").addEventListener("keyup", areaTexto());
  
}


function extrairTexto() {
  var caixaTexto = document.querySelector("#caixaTexto").value;

  var hora = horaDoTwitter();

  var twitter = montarTwitter(caixaTexto, hora);
  console.log(twitter);

  mostrarTwitters(twitter);
  document.getElementById('textos').reset();
    return false;
  
}

function horaDoTwitter() {
  var d = new Date();
  return d.toLocaleTimeString();
}

function montarTwitter(mensagem, hora) {
  var twitterCompleto = {
    mensagem: mensagem,
    hora: hora
  };
  return twitterCompleto;
  
}

function mostrarTwitters(twitter) {
  var criarElementoP = document.createElement("p");

  var msgTwitter = document.createTextNode(
    (document.querySelector("#msgRetornoTwiter").value = horaDoTwitter() + ' ' + twitter.mensagem) 
  );

  criarElementoP.appendChild(msgTwitter);
  document
    .getElementById("msgRetornoTwiter")
    .appendChild(criarElementoP).style.backgroundColor =
    "White";
    
}

document.querySelector("button").addEventListener("click", extrairTexto);

contadorRegressivo();

function areaTexto() {
  objTextArea = document.getElementById("caixaTexto");
  while (objTextArea.scrollHeight > objTextArea.offsetHeight) {
    objTextArea.rows += 1;
  }
}
